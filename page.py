#http://www.scorespro.com/rss2/live-soccer.xml

from google.appengine.api import users
import webapp2
import jinja2
import os

JINJA_ENVIRONMENT = jinja2.Environment(autoescape=True,
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__))))

class MainPage(webapp2.RequestHandler):
    def get(self):
		template = JINJA_ENVIRONMENT.get_template('template/pageview.html')

		self.response.write(template.render())
        
#test comment
		
app = webapp2.WSGIApplication([('/', MainPage),], debug=True)